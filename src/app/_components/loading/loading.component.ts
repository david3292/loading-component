import { Component, OnInit } from '@angular/core';
import { LoadingService } from 'src/app/_service/loading.service';

@Component({
  selector: 'app-loading',
  templateUrl: './loading.component.html',
  styleUrls: ['./loading.component.sass']
})
export class LoadingComponent implements OnInit {

  private showLoading: boolean = false

  constructor(
    private _loadingService: LoadingService
  ) { }

  ngOnInit(): void {

    this._loadingService.changeStatusLoading$.subscribe(data => this.showLoading = data);

  }

  hasShowLoading() {
    if (this.showLoading) {
      document.body.classList.add('load');
      return ['loading'];
    } else {
      document.body.classList.remove('load');
      return ['no-loading'];
    }
  }


}
