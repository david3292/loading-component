import { Component } from '@angular/core';
import { LoadingService } from './_service/loading.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent {
  title = 'loading-component';

  constructor(
    private _loadingService: LoadingService
  ){ }

  activarLoading(){
    this._loadingService.changeStatusLoading$.next(true);
    setTimeout(() => {
      this._loadingService.changeStatusLoading$.next(false);
    }, 5000);

  }

}
